# README #

Pure-python algorithms for trees.

Tree traversal and manipulation 'like-a-boss' ?!
[ ![Codeship Status for sys-git/pine](https://codeship.com/projects/f1276c50-b050-0132-57b0-124419f0ca52/status?branch=master)](https://codeship.com/projects/69517)
[![Build Status](https://api.shippable.com/projects/550a954b5ab6cc1352a4d6f8/badge?branchName=master)](https://app.shippable.com/projects/550a954b5ab6cc1352a4d6f8/builds/latest)

[![Downloads](https://pypip.in/download/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Latest Version](https://pypip.in/version/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Supported Python versions](https://pypip.in/py_versions/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Supported Python implementations](https://pypip.in/implementation/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Development Status](https://pypip.in/status/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Wheel Status](https://pypip.in/wheel/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Egg Status](https://pypip.in/egg/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Download format](https://pypip.in/format/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![License](https://pypip.in/license/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)


### How do I get set up? ###

* **python setup.py install**
* Dependencies: **None**
* Dependencies (test):  **Coverage, nose**
* How to run tests:  **./runtests.sh**
* Deployment instructions:  **pip install pine**

### Contribution guidelines ###
I accept pull requests.

### What about test coverage? ###
There is a full suite of unit-tests.

### Who do I talk to? ###

* Francis Horsman:  **francis.horsman@gmail.com**

### Example ###

```
>>> from pine import pine


```
