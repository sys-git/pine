README
======

Pure-python algorithms for trees.
Tree traversal and manipulation 'like-a-boss'

.. image:: https://codeship.com/projects/f1276c50-b050-0132-57b0-124419f0ca52/status?branch=master
    :target: https://codeship.com/projects/69517
    :alt: Codeship Status
.. image:: https://api.shippable.com/projects/550a954b5ab6cc1352a4d6f8/badge?branchName=master
    :target: https://app.shippable.com/projects/550a954b5ab6cc1352a4d6f8/builds/latest
    :alt: Build Status
.. image:: https://pypip.in/download/pine/badge.svg
    :target: https://pypi.python.org/pypi/pine/
    :alt: Downloads
.. image:: https://pypip.in/version/pine/badge.svg
    :target: https://pypi.python.org/pypi/pine/
    :alt: Latest Version
.. image:: https://pypip.in/py_versions/pine/badge.svg
    :target: https://pypi.python.org/pypi/pine/
    :alt: Supported Python versions
.. image:: https://pypip.in/status/pine/badge.svg
    :target: https://pypi.python.org/pypi/pine/
    :alt: Development Status
.. image:: https://pypip.in/wheel/pine/badge.svg
    :target: https://pypi.python.org/pypi/pine/
    :alt: Wheel Status
.. image:: https://pypip.in/egg/pine/badge.svg
    :target: https://pypi.python.org/pypi/pine/
    :alt: Egg Status
.. image:: https://pypip.in/format/pine/badge.svg
    :target: https://pypi.python.org/pypi/pine/
    :alt: Download format
.. image:: https://pypip.in/license/pine/badge.svg
    :target: https://pypi.python.org/pypi/pine/
    :alt: License

How do I get set up?
~~~~~~~~~~~~~~~~~~~~

-  **python setup.py install**
-  Dependencies: **six**
-  Dependencies (test): **Coverage, nose**
-  How to run tests: **./runtests.sh**
-  Deployment instructions: **pip install pine**

Contribution guidelines
~~~~~~~~~~~~~~~~~~~~~~~

I accept pull requests.

What about test coverage?
~~~~~~~~~~~~~~~~~~~~~~~~~

There is a full suite of unit-tests.

Who do I talk to?
~~~~~~~~~~~~~~~~~

-  Francis Horsman: **francis.horsman@gmail.com**

Example
~~~~~~~

::

    >>> from pine import pine


