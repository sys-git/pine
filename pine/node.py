# -*- coding: utf-8 -*-
"""
Pine.
"""

from collections import namedtuple

ImmutableTreeNode = namedtuple('Tree', ('value', 'left_node', 'right_node'))


def is_tip(node):
    return node.left_node is node.right_node is None


class TreeNode(object):
    def __init__(self, value, left_node=None, right_node=None):
        if isinstance(value, ImmutableTreeNode):
            left_node = left_node if left_node is not None else value.left_node
            right_node = right_node if right_node is not None else \
                value.right_node
            value = value.value
        self._x = value
        self._l = left_node
        self._r = right_node

    @property
    def is_tip(self):
        return is_tip(self)

    @property
    def value(self):
        return self._x

    @property
    def left_node(self):
        return self._l

    @property
    def right_node(self):
        return self._r

    def __str__(self):
        return 'TreeNode%s(%s)' % ('-leaf' if self.is_tip else '', self.value)


if __name__ == '__main__':  # pragma no cover
    pass
