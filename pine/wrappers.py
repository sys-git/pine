# -*- coding: utf-8 -*-
"""
Pine standalone wrappers.
"""

from pine.core import walk_paths
from pine.stats import TreeStats


def amplitude(tree):
    """
    Determine the min and max amplitude of a given binary tree.

    :type tree: TreeNode, Tree, representing a binary tree,
        and is valid (no circular dependencies).
    :return: Amplitude, The (maximum, minimum) amplitude of the tree.
    """
    l = TreeStats(tree, store_paths=False)
    walk_paths(tree, **l.methods)
    print('amplitude: ', l.amplitude)
    return l.amplitude


def length(tree):
    """
    Determine the length of paths of a given binary tree.

    :type tree: TreeNode, Tree, representing a binary tree,
        and is valid (no circular dependencies).
    :return: Length, The (maximum, minimum) length of the tree.
    """
    l = TreeStats(tree, store_paths=True)
    walk_paths(tree, **l.methods)
    print('length: ', l.length)
    return l.length


def value(tree):
    """
    Determine the min and max value of a given binary tree.

    :type tree: TreeNode, Tree, representing a binary tree,
        and is valid (no circular dependencies).
    :return: Value, The (maximum, minimum) value of the tree.
    """
    l = TreeStats(tree, store_paths=True)
    walk_paths(tree, **l.methods)
    print('value: ', l.value)
    return l.value


def paths(tree):
    """
    Determine the min and max amplitude of a given binary tree.

    :type tree: TreeNode, Tree, representing a binary tree,
        and is valid (no circular dependencies).
    :return: Amplitude, The (maximum, minimum) amplitude of the tree.
    """
    l = TreeStats(tree, store_paths=True)
    walk_paths(tree, **l.methods)
    print('paths: ', l.paths)
    return l.paths


def num_paths(tree):
    """
    Determine the number of paths of a given binary tree.

    :type tree: TreeNode, Tree, representing a binary tree,
        and is valid (no circular dependencies).
    :return: int, number of paths
    """
    l = TreeStats(tree, store_paths=True)
    walk_paths(tree, **l.methods)
    print('num_paths: ', l.num_paths)
    return l.num_paths


if __name__ == '__main__':  # pragma no cover
    pass
