# -*- coding: utf-8 -*-
"""
Pine.
"""

from collections import namedtuple

import six

from pine.errors import ParamValueError, ParamTypeError
from pine.node import TreeNode, ImmutableTreeNode


Amplitude = namedtuple('Amplitude', ('min', 'max'))
Length = namedtuple('Length', ('min', 'max'))
Value = namedtuple('Value', ('min', 'max'))


def walk_paths(tree, on_left=None, on_right=None, on_leaf=None):
    """
    Walk the tree and return the paths from tip-to-leaf.

    :type tree: TreeNode, Tree, representing a binary tree.
    :type on_left: Callable, Called when processing of a node's left branch is
    complete.
    :type on_right: Callable, Called when processing of a node's right branch
    is complete.
    :type on_leaf: Callable, Called when processing of a leaf-node is complete.
    :return: None
    """
    if not isinstance(tree, (TreeNode, ImmutableTreeNode)):  # pragma no cover
        raise ParamTypeError('tree is incorrect type: %s' % tree)
    if on_left and not six.callable(on_left):  # pragma no cover
        raise ParamValueError('on_left must be a callable,got: %s' % on_left)
    if on_right and not six.callable(on_left):  # pragma no cover
        raise ParamValueError('on_right must be a callable,got: %s' % on_right)
    if on_leaf and not six.callable(on_leaf):  # pragma no cover
        raise ParamValueError('on_leaf must be a callable,got: %s' % on_leaf)

    _parse_node(tree, [], on_left=on_left, on_right=on_right, on_leaf=on_leaf)


def _parse_node(node, path, on_left=None, on_right=None, on_leaf=None):
    path.append(node)

    if node.left_node is not None:
        # Take the Red pill:
        _parse_node(node.left_node, path, on_left=on_left, on_right=on_right,
                    on_leaf=on_leaf)
        on_left and on_left(path)
        path.pop(-1)
    if node.right_node is not None:
        # Take the Green pill:
        _parse_node(node.right_node, path, on_left=on_left, on_right=on_right,
                    on_leaf=on_leaf)
        on_right and on_right(path)
        path.pop(-1)
    if node.left_node is node.right_node is None:
        # Take the path to the telephone:
        on_leaf and on_leaf(path)


if __name__ == '__main__':  # pragma no cover
    pass
