# -*- coding: utf-8 -*-
"""
Errors.
"""


class PineError(Exception):
    pass


class ParamValueError(PineError, ValueError):
    pass


class ParamTypeError(PineError, TypeError):
    pass


if __name__ == '__main__':  # pragma no cover
    pass
