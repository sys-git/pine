# -*- coding: utf-8 -*-
"""
Pine.
"""

from pine.stats import TreeStats
from pine.core import walk_paths


class pine(object):
    """
    An encapsulation of a Tree and it's statistics.
    """

    def __init__(self, tree, lazy=True, store_paths=True):
        self._tree = tree
        self._stats = TreeStats(self.tree, store_paths=store_paths)
        self._store_paths = store_paths
        self._walked = False
        if not lazy:
            self.walk()

    def walk(self, **kwargs):
        store_paths = kwargs.get('store_paths', self._store_paths)

        self._stats = TreeStats(self.tree, store_paths=store_paths)
        walk_paths(self.tree, **self._stats.methods)
        self._walked = True

    @property
    def has_walked(self):
        return self._walked

    @property
    def tree(self):
        return self._tree

    @property
    def stats(self):
        return self._stats

    @property
    def amplitude(self):
        return self.stats.amplitude

    @property
    def length(self):
        return self.stats.length

    @property
    def value(self):
        return self.stats.value

    @property
    def paths(self):
        return self.stats.paths

    @property
    def num_paths(self):
        return self.stats.num_paths


if __name__ == '__main__':  # pragma no cover
    pass
