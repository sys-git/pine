# -*- coding: utf-8 -*-
"""
Pine standalone wrappers.
"""

import copy

from pine.core import Amplitude, Length, Value


class TreeStats(object):
    """
    A container for tree statistics.
    """

    def __init__(self, tree, store_paths=False):
        self._tree = tree
        self._len_longest = None
        self._len_shortest = None
        self._amplitude_min = None
        self._amplitude_max = None
        self._value_min = None
        self._value_max = None
        self._store_paths = store_paths
        self._paths = 0 if store_paths is False else []
        self._empty = True

    def on_left(self, path):
        # print('on_left: %s' % path)
        pass

    def on_right(self, path):
        # print('on_right: %s' % path)
        pass

    def on_leaf(self, path):
        # print('%s yields new path: %s' % path)

        self._empty = False
        if self._store_paths:
            self._paths.append(copy.deepcopy(path))
        else:
            self._paths += 1

        path_values = [p.value for p in path]

        self._value_min = min(self._value_min, min(
            path_values)) if self._value_min is not None else min(path_values)
        self._value_max = max(self._value_max, max(
            path_values)) if self._value_max is not None else max(path_values)

        self._len_longest = max(self._len_longest, len(
            path_values)) if self._len_longest is not None else \
            len(path_values)
        self._len_shortest = min(self._len_shortest, len(
            path_values)) if self._len_shortest is not None else len(
            path_values)

        amp = max(path_values) - min(path_values)
        self._amplitude_max = max(self._amplitude_max,
                                  amp) if self._amplitude_max is not None \
            else amp
        self._amplitude_min = min(self._amplitude_min,
                                  amp) if self._amplitude_min is not None \
            else amp

    @property
    def empty(self):
        return self._empty

    @property
    def methods(self):
        return dict(on_left=self.on_left, on_right=self.on_right,
                    on_leaf=self.on_leaf)

    @property
    def amplitude(self):
        return Amplitude(self._amplitude_min, self._amplitude_max)

    @property
    def length(self):
        return Length(self._len_shortest, self._len_longest)

    @property
    def value(self):
        return Value(self._value_min, self._value_max)

    @property
    def num_paths(self):
        return self._paths if not self.store_paths else len(self._paths)

    @property
    def store_paths(self):
        return self._store_paths

    @property
    def paths(self):
        result = []
        if self.store_paths:
            for path in self._paths:
                result.append([str(p) for p in path])
        return result

    @property
    def tree(self):
        return self._tree

    @property
    def stats(self):
        return dict(paths=self.paths, num_paths=self.num_paths,
                    value=self.value, length=self.length,
                    amplitude=self.amplitude)

    def __str__(self):
        s = []
        s.append('Paths(%s)' % self.num_paths)
        s.append(
            'Amplitudes(%s, %s)' % (self.amplitude.min, self.amplitude.max))
        s.append('Values(%s, %s)' % (self.value.min, self.value.max))
        s.append('Lengths(%s, %s)' % (self.length.min, self.length.max))
        return 'Tree: ' + ', '.join(s)


if __name__ == '__main__':  # pragma no cover
    pass
