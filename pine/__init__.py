# -*- coding: utf-8 -*-
"""
Pine.
"""

__author__ = 'Francis Horsman'
__version__ = '0.0.1'
__url__ = 'https://bitbucket.org/sys-git/pine'
__email__ = 'francis.horsman@gmail.com'
__short_description__ = 'Pure-python algorithms for trees'
__synopsis__ = '.\nTraverse and manipulate trees.'
__long_description__ = __short_description__ + __synopsis__

from pine.impls import pine  # NOQA
from pine.node import TreeNode, ImmutableTreeNode, is_tip  # NOQA
from pine.errors import ParamTypeError, ParamValueError, PineError  # NOQA
from pine.wrappers import amplitude, length, value, paths, num_paths  # NOQA

if __name__ == '__main__':  # pragma no cover
    pass
