#!/usr/bin/env python
#    -*- coding: utf-8 -*-

import os

try:
    from setuptools import setup, find_packages
except ImportError:
    import ez_setup
    ez_setup.use_setuptools()
    from setuptools import setup, find_packages

#    Allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

import pine

setup(
    name="pine",
    packages=find_packages(exclude=('tests',)),
    version=pine.__version__,
    url=pine.__url__,
    author=pine.__author__,
    author_email=pine.__email__,
    description=pine.__short_description__,
    license="GNU General Public License",
    platforms=['any'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    zip_safe=False,
    keywords='algorithms',
    install_requires=[k for k in open('requirements.txt').readlines() if k],
)
