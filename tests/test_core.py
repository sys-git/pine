# -*- coding: utf-8 -*-
"""
Test pine.
"""

import unittest

from pine.core import TreeNode, ImmutableTreeNode, Amplitude, Length, Value
from pine.impls import pine
from pine.stats import TreeStats
from pine.wrappers import amplitude, paths, num_paths, length, value
from pine.node import is_tip


class test_core(unittest.TestCase):
    def setUp(self):
        self.tree = TreeNode(5, TreeNode(8, TreeNode(12, None, None),
                                         TreeNode(2, None, None)),
                             TreeNode(9, TreeNode(7, TreeNode(1, None, None),
                                                  None),
                                      TreeNode(4, TreeNode(3, None, None),
                                               None)))

    def test_empty_tree_stats(self):
        self.assertEquals(str(TreeStats(self.tree, store_paths=True)),
                          'Tree: Paths(0), Amplitudes(None, None), '
                          'Values(None, None), Lengths(None, None)')

    def test_amplitude(self):
        a = amplitude(self.tree)
        self.assertEqual(a.max, 8)
        self.assertEqual(a.min, 6)

    def test_length(self):
        l = length(self.tree)
        self.assertEqual(l.max, 4)
        self.assertEqual(l.min, 3)

    def test_value(self):
        v = value(self.tree)
        self.assertEqual(v.max, 12)
        self.assertEqual(v.min, 1)

    def test_paths(self):
        self.assertEquals(len(paths(self.tree)), 4)

    def test_num_paths(self):
        self.assertEqual(num_paths(self.tree), 4)

    def test_impl_lazy(self):
        p = pine(self.tree, lazy=True, store_paths=False)
        assert not p.has_walked
        assert p.tree is self.tree
        assert isinstance(p.stats, TreeStats)
        assert p.stats.empty is True
        assert isinstance(p.stats.amplitude, Amplitude)
        assert isinstance(p.stats.value, Value)
        assert isinstance(p.stats.length, Length)
        assert isinstance(p.stats.stats, dict)
        assert isinstance(p.stats.methods, dict)
        assert p.stats.paths == []
        assert p.stats.store_paths is False
        assert p.stats.num_paths == 0

        p.walk(store_paths=True)
        assert p.has_walked is True
        assert p.amplitude == p.stats.amplitude
        assert p.value == p.stats.value
        assert p.length == p.stats.length

        assert p.amplitude.min == 6
        assert p.amplitude.max == 8
        assert p.value.min == 1
        assert p.value.max == 12
        assert p.length.min == 3
        assert p.length.max == 4

        assert p.stats.empty is False

        assert len(p.paths) == 4
        assert p.num_paths == 4

    def test_impl_lazy_no_store_paths(self):
        p = pine(self.tree, lazy=True, store_paths=False)
        assert not p.has_walked
        assert p.tree is self.tree
        assert isinstance(p.stats, TreeStats)
        assert p.stats.empty is True
        assert isinstance(p.stats.amplitude, Amplitude)
        assert isinstance(p.stats.value, Value)
        assert isinstance(p.stats.length, Length)
        assert isinstance(p.stats.stats, dict)
        assert isinstance(p.stats.methods, dict)
        assert p.stats.paths == []
        assert p.stats.store_paths is False
        assert p.stats.num_paths == 0

        p.walk(store_paths=False)
        assert p.has_walked is True
        assert p.amplitude == p.stats.amplitude
        assert p.value == p.stats.value
        assert p.length == p.stats.length

        assert p.amplitude.min == 6
        assert p.amplitude.max == 8
        assert p.value.min == 1
        assert p.value.max == 12
        assert p.length.min == 3
        assert p.length.max == 4

        assert p.stats.empty is False

        assert len(p.paths) == 0
        assert p.num_paths == 4

    def test_impl_non_lazy(self):
        p = pine(self.tree, lazy=False, store_paths=True)
        assert p.has_walked
        assert p.tree is self.tree
        assert isinstance(p.stats, TreeStats)
        assert p.stats.empty is False
        assert isinstance(p.stats.amplitude, Amplitude)
        assert isinstance(p.stats.value, Value)
        assert isinstance(p.stats.length, Length)
        assert isinstance(p.stats.stats, dict)
        assert isinstance(p.stats.methods, dict)
        assert len(p.stats.paths) == 4
        assert p.stats.store_paths is True
        assert p.stats.num_paths == 4

        assert p.amplitude == p.stats.amplitude
        assert p.value == p.stats.value
        assert p.length == p.stats.length

        assert p.amplitude.min == 6
        assert p.amplitude.max == 8
        assert p.value.min == 1
        assert p.value.max == 12
        assert p.length.min == 3
        assert p.length.max == 4

        assert p.stats.empty is False

    def test_impl_non_lazy_no_store_paths(self):
        p = pine(self.tree, lazy=False, store_paths=False)
        assert p.has_walked
        assert p.tree is self.tree
        assert isinstance(p.stats, TreeStats)
        assert p.stats.empty is False
        assert isinstance(p.stats.amplitude, Amplitude)
        assert isinstance(p.stats.value, Value)
        assert isinstance(p.stats.length, Length)
        assert isinstance(p.stats.stats, dict)
        assert isinstance(p.stats.methods, dict)
        assert p.stats.tree is p.tree is self.tree
        assert len(p.stats.paths) == 0
        assert p.stats.store_paths is False
        assert p.stats.num_paths == 4

        assert p.amplitude == p.stats.amplitude
        assert p.value == p.stats.value
        assert p.length == p.stats.length

        assert p.amplitude.min == 6
        assert p.amplitude.max == 8
        assert p.value.min == 1
        assert p.value.max == 12
        assert p.length.min == 3
        assert p.length.max == 4

        assert p.stats.empty is False

    def test_immutable_tree_node(self):
        a = ImmutableTreeNode(1, None, None)
        b = ImmutableTreeNode(2, None, None)
        c = ImmutableTreeNode(3, a, b)
        assert is_tip(a)
        assert is_tip(b)
        assert not is_tip(c)
        b = ImmutableTreeNode(0, None, None)
        t = ImmutableTreeNode(1, a, b)

    def test_tree_node_constructor(self):
        a = ImmutableTreeNode(1, None, None)
        b = ImmutableTreeNode(2, None, None)
        c = ImmutableTreeNode(3, a, b)
        d = TreeNode(c)
        assert d.left_node is a
        assert d.right_node is b
        assert d.value == 3

        e = TreeNode(c, left_node=b, right_node=a)
        assert e.left_node is b
        assert e.right_node is a
        assert e.value == 3


if __name__ == '__main__':  # pragma no cover
    unittest.main()

