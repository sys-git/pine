#!/bin/sh

export PYTHONPATH=.
flake8 pine/*.py
nosetests --exe -v -s --nologcapture --cover-inclusive --with-coverage --cover-package=pine --cover-erase --cover-html-dir=~/.coverage --cover-branches --with-xunit tests
